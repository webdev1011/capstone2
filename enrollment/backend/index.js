const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/user');
const courseRoutes = require('./routes/course');
//require and configure .env
require('dotenv').config()

//connect to mongoDB
const connectionstring = process.env.DB_CONNECTION_STRING
mongoose.connection.once('open', ()=> console.log('Now connected to mongoDB Atlas.'));
mongoose.connect(connectionstring,
	{ useNewUrlParser: true,
	useUnifiedTopology: true,
	})
const app = express()

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true}))

//Routes
app.use('/app/users', userRoutes)
app.use('/app/course', courseRoutes)


//save environment variables
const port = process.env.PORT;
app.listen(process.env.PORT, () => console.log(`You got served on port ${process.env.PORT}!`));
