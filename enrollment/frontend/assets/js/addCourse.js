let addCourse = document.querySelector('#createCourse');

addCourse.addEventListener('submit', (e) => {
	e.preventDefault()

	let name = document.querySelector('#courseName').value;
	let description = document.querySelector('#courseDescription').value;
	let price = document.querySelector('#coursePrice').value;

	let token = localStorage.getItem('token');


	fetch('http://localhost:4000/app/course', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if(data === true) {
				alert('Course succesfully created!')
				window.location.replace("./courses.html")
			} else {
				alert("Something went wrong.")

			}
		})

})