//window.location.search returns the query string part of URL


//console.log(window.location.search)

let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem('token')


// console.log(courseId)
let courseName = document.querySelector("#courseName")
let courseDescription = document.querySelector("#courseDescription")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")


fetch(`http://localhost:4000/app/course/${courseId}`)
.then(res => res.json())
.then(data => {
	console.log(data)

	courseName.innerHTML = data.name
	courseDescription.innerHTML = data.description
	coursePrice.innerHTML = data.price
	enrollContainer.innerHTML = 
		`
			<button id="enrollButton" class="btn btn-block btn-success">
				Enroll
			</button>
		`

	document.querySelector("#enrollButton").addEventListener("click", (e) =>{
		e.preventDefault
		//insert the course to our courses collection
		fetch('http://localhost:4000/app/users/enroll', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data === true) {
				alert('You are now enrolled!')
				window.location.replace("./courses.html")
			} else {
				alert("Something went wrong.")

			}
		})
	})

})

