let adminUser = localStorage.getItem("isAdmin");

let cardFooter;

fetch('http://localhost:4000/app/course/')
.then(res => res.json())
.then(data => {
	console.log(data)

	let courseData;

	if(data.length < 1) {
		courseData = "No courses available."
	
	} else {

		courseData = data.map(course => {
			console.log(course._id)

			if(adminUser === "false" || !adminUser ) {
				cardFooter =
				`
					<a href="./course.html?courseId=${course._id}"value="{course.id}" class="btn btn-success text-black btn-block editButton">
					Select Course
					</a>

				`
			} else {
				cardFooter =
				`
					<a href="./enrollees.html?courseId=${course._id}"value="{course._id}" class="btn btn-success text-black btn-block dangerButton">Enrollees</a>
					<a href="./editCourse.html?courseId=${course._id}"value="{course._id}" class="btn btn-warning text-black btn-block editButton">Edit</a>
					<a href="./deleteCourse.html?courseId=${course._id}"value="{course._id}" class="btn btn-danger text-black btn-block dangerButton">Disable</a>
				
				`
			}
			return(


				`
					<div class="col-md-6 my-3 card text-black bg-primary">
						<div class="card text-center">
						  <div class="card-header bg-primary">
   								 Course 
							  </div>
							<div class="card-body">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">${course.description}</p>
								<p class="card-text text-right">Price: ₱${course.price}</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
				`

			)
		}).join("");
	}
	let container = document.querySelector("#coursesContainer")

	container.innerHTML = courseData;

})

let modalButton = document.querySelector("#adminButton")

if(adminUser == "false" || !adminUser) {
	modalButton.innerHTML = null
} else {
	modalButton.innerHTML =
	`
		<div class="d-grid gap-2 d-md-block">
			<a href="./addCourse.html" class="btn btn-success" type="button">Add Course</a>
			<a href="./archive.html" class="btn btn-info" type="button">Archive</a>
		</div>
	`
}

