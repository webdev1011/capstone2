let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem('token')


fetch(`http://localhost:4000/app/course/${courseId}`, {
		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			courseId,
		})
	})
	.then(res => {
		return res.json()
	})
	.then(data => {
		console.log(data)

		if(data === true) {
			alert('Course now disabled. You may delete or restore Course through Archives')
			window.location.replace("./courses.html")
		} else {

			alert("Something went wrong.")

		}
	})



