let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem('token')

let courseName = document.querySelector("#courseName")
let courseDescription = document.querySelector("#courseDescription")

fetch(`http://localhost:4000/app/course/${courseId}`)

.then(res => res.json())
.then(data => {
	console.log(data)

	courseName.innerHTML = data.name
	courseDescription.innerHTML = data.description

	let enrolleesData = data.enrollees
	let enrollmentsInfo = [];
	let enrollContainer = document.querySelector("#enrollContainer")

	for (let student of enrolleesData) {
		fetch(`http://localhost:4000/app/users/${student.userId}`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
				}
		})
		.then(res => res.json())
		.then(data =>{
			enrollContainer.innerHTML = "";
			enrollmentsInfo.push({firstName:data.firstName, lastName:data.lastName});

			for(let i = 0; i < enrollmentsInfo.length; i++){
				console.log(enrollmentsInfo[i].firstName)
				const newLi = document.createElement('li');
				newLi.append(`${enrollmentsInfo[i].firstName} ${enrollmentsInfo[i].lastName}`);
				enrollContainer.append(newLi);
			}
		})

	}
	if (enrollmentsInfo.length < 1) {
				enrollContainer.innerHTML = "No enrollees!"

			}


})