let loginForm = document.querySelector("#logInUser");

loginForm.addEventListener("submit", (e) => {
	e.preventDefault();

	let email = document.querySelector("#userEmail").value;
	let password = document.querySelector("#password").value;

	if (email == "" || password == ""){
		alert("Please input your email and/or password.")
	} else {
		fetch('http://localhost:4000/app/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data.accessToken) {
				localStorage.setItem('token', data.accessToken);
				fetch('http://localhost:4000/app/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)
					window.location.replace("./courses.html")
				})
			} else {
				alert("Something went wrong!");
			}
		})
	}

})

