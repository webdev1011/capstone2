
let params = new URLSearchParams(window.location.search)
let token = localStorage.getItem('token')


let firstName = document.querySelector("#firstName")
let lastName = document.querySelector("#lastName")
let email = document.querySelector("#email")
let mobileNo = document.querySelector("#mobileNo")
let enrollments = document.querySelector("#classes")


fetch(`http://localhost:4000/app/users/details`, {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
		}
	})

	.then(res => res.json())
	.then(data => {
		console.log(data)

			firstName.innerHTML = data.firstName
			lastName.innerHTML = data.lastName
			email.innerHTML = data.email
			mobileNo.innerHTML = data.mobileNo
	
			let subjectContainer =document.querySelector("#classes")
			let subjectData = data.enrollments
			let courseInfo = [];
				

			for(let subject of subjectData){
				fetch(`http://localhost:4000/app/course/${subject.courseId}`)
				.then(res => res.json())
				.then(data =>{

					subjectContainer.innerHTML ="";
					courseInfo.push({courseName:data.name, courseDescription:data.description});

					for(let i = 0 ; i < courseInfo.length ; i++){
					console.log(courseInfo[i].courseName)
					const newLi = document.createElement('li');
					newLi.append(`${courseInfo[i].courseName} : ${courseInfo[i].courseDescription}`);
					subjectContainer.append(newLi);

					}

				})
				
			}

			if (courseInfo.length < 1) {
				subjectContainer.innerHTML = "No classes enrolled"

			}
		

	})


